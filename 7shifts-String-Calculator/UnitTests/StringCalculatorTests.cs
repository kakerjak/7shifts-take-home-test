using _7shifts_String_Calculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTests
{
    [TestClass]
    public class StringCalculatorTests
    {
        [TestMethod]
        public void Add_ShouldReturnZeroWhenParameterIsEmpty()
        {
            var strCalc = new StringCalculator();

            var result = strCalc.Add("");

            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void Add_ShouldReturnSumOfIntegers()
        {
            var strCalc = new StringCalculator();

            var result = strCalc.Add("1,2,5");

            Assert.AreEqual(8, result);
        }

        [TestMethod]
        public void Add_ShouldStripPreceedingNewlineCharactersAndReturnSumOfIntegers()
        {
            var strCalc = new StringCalculator("\n");

            var result = strCalc.Add("1\n,2,3");

            Assert.AreEqual(6, result);
        }

        [TestMethod]
        public void Add_ShouldStripFollowingNewlineCharactersAndReturnSumOfIntegers()
        {
            var strCalc = new StringCalculator("\n");

            var result = strCalc.Add("1,\n2,4");

            Assert.AreEqual(7, result);
        }

        [TestMethod]
        public void Add_ShouldUseCustomDelimiters()
        {
            var strCalc = new StringCalculator("\n");

            var result = strCalc.Add("//@\n2@3@8");

            Assert.AreEqual(13, result);
        }

        [TestMethod]
        public void Add_InputWithNegativesShouldThrowException()
        {
            var strCalc = new StringCalculator("\n");

            Assert.ThrowsException<ApplicationException>(() => strCalc.Add("//@\n24@-35@877"));
        }

        [TestMethod]
        public void Add_ShouldIgnoreNumbersGreaterThan1000()
        {
            var strCalc = new StringCalculator();

            var result = strCalc.Add("4,5,6,10035");

            Assert.AreEqual(15, result);
        }

        [TestMethod]
        public void Add_ShouldAllowMultipleCustomDelimitersOfVariableLength()
        {
            var strCalc = new StringCalculator("\n");

            var result = strCalc.Add("//@,#%\n2@3@8#%50#%20@7");

            Assert.AreEqual(90, result);
        }
    }
}
