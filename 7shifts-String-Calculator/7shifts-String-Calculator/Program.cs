﻿using System;
using System.Text;

namespace _7shifts_String_Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            var strCalc = new StringCalculator();

            Console.WriteLine("7shifts String Calculator");
            Console.WriteLine("-------------------------");

            do
            {
                StringBuilder data = new StringBuilder("");

                Console.WriteLine("\nString Calculator Add method");
                Console.WriteLine("Enter your input value and enter 'done' on a newline to calculate:");

                string input;
                while ((input = Console.ReadLine()) != "done")
                {
                    data.Append(input);
                    data.Append(Environment.NewLine);
                }
                try
                {
                    var sum = strCalc.Add(data.ToString());
                    Console.WriteLine($"The sum of the integers provided [{data.ToString().Replace(Environment.NewLine, "\\n")}] is {sum}.");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"ERROR: {e.Message}");
                }

                Console.WriteLine("Press q to quit, or any other key to perform another calculation.");

            } while (Console.ReadLine() != "q");

            Environment.Exit(0);
        }
    }
}
