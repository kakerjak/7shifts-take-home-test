﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _7shifts_String_Calculator
{
    public class StringCalculator
    {
        string _newline;

        public StringCalculator(string newline = "\r\n")
        {
            _newline = newline;
        }

        public int Add(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return 0;
            }

            string delim = ",";
            List<string> delims = new List<string>();

            if (input.Substring(0,2) == "//")
            {
                int newlinePos = input.IndexOf(_newline);
                delim = input.Substring(2, newlinePos - 2);
                if (delim.IndexOf(',') >= 0)
                {
                    delims = delim.Split(',').ToList();
                }
                input = input.Substring(newlinePos);
            }
            
            input = input.Replace(_newline, "");

            if (string.IsNullOrEmpty(input))
            {
                return 0;
            }

            if (!delims.Any())
            {
                delims.Add(delim);
            }
            var splitters = delims.ToArray();

            List<int> ints = input.Split(splitters, StringSplitOptions.None)
                                  .Where(x => !string.IsNullOrEmpty(x))
                                  .Select(int.Parse)
                                  .Where(x => x <= 1000)
                                  .ToList();

            List<int> negativeInts = ints.Where(x => x < 0).ToList();

            if (negativeInts.Any())
            {
                throw new ApplicationException($"Negatives not allowed! The following negative numbers were found in the input string [{string.Join(delim, negativeInts.Select(x => x.ToString()).ToArray())}]");
            }

            return ints.Sum();
        }
    }
}
